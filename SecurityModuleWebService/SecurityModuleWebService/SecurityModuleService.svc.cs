﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace SecurityModuleWebService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "SecurityModuleService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select SecurityModuleService.svc or SecurityModuleService.svc.cs at the Solution Explorer and start debugging.
    public class SecurityModuleService : ISecurityModuleService
    {
        Security_moduleDBEntities SMDB = new Security_moduleDBEntities();
        

        List<user> ISecurityModuleService.ListUser()
        {
            return SMDB.users.ToList<user>();
        }
    }
}
